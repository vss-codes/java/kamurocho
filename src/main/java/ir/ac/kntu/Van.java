package ir.ac.kntu;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Van extends HQ {

    public Van() {
	durability = 6000;
	shape.setWidth(35);
	shape.setHeight(75);
	shape.setFill(Color.YELLOW);
	Main.root.getChildren().add(shape);
	shape.setVisible(false);
    }

    public Van(double x, double y) {
	this();
	show(x, y);
    }
}
