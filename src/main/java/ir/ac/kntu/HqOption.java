package ir.ac.kntu;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class HqOption extends Option {
    private HQ hq;
    private static Text dur, newDur;
    private Text level, cost;
    private int price;

    public HqOption(double x, double y, String text, HQ hq) {
	super(x, y, text);
	this.hq = hq;
	price = hq.lvl * 100;
	level = new Text(x + 120, y + 25, "lvl: " + hq.lvl);
	level.setFont(Font.font(20));
	cost = new Text(x + 210, y + 25, "cost: " + price);
	cost.setFont(Font.font(20));
	level.setMouseTransparent(true);
	cost.setMouseTransparent(true);
	dur = new Text(610, 125, "");
	dur.setFont(Font.font(20));
	newDur = new Text(607, 175, "");
	newDur.setFill(Color.LIME);
	newDur.setFont(Font.font(20));
	root.getChildren().addAll(level, cost, dur, newDur);

	field.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
	    hq.show(500 + (100 - hq.shape.getWidth()) / 2, 100 + (100 - hq.shape.getHeight()) / 2);
	    dur.setText("durability: \n" + hq.durability);
	    newDur.setText("+" + (int) Math.floor(hq.durability * 0.1));
	});

	field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
	    if (Game.coins >= price) {
		updgrade(this.hq);
	    } else {
		Buy.notEnough();
	    }
	});
    }

    private void updgrade(HQ hq) {
	hq.durability *= 1.1;
	Game.coins -= price;
	price = ++hq.lvl * 100;
	Game.coins();
	level.setText("lvl: " + hq.lvl);
	cost.setText("cost: " + price);
    }

    public void update(Shape shape) {
	root.getChildren().remove(shape);
	root.getChildren().add(shape);
    }

}
