package ir.ac.kntu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
    static Group root = new Group();
    static Scene scene = new Scene(root, 800, 600, false, SceneAntialiasing.BALANCED);
    private static List<Option> options;

    @Override
    public void start(Stage stage) {
	Soldier.createAll(Game.players, Game.enemies);
	stage.setScene(scene);
	stage.setTitle("Once Upon a Time in Kamurocho!");
	stage.setResizable(false);
	scene.setFill(Color.DARKGREEN);
	Option missions = new MenuOption(100, 100, "Missions");
	Option organization = new MenuOption(100, 150, "Organizations");
	Option train = new MenuOption(100, 200, "Train");
	Option fortify = new MenuOption(100, 250, "Fortify HQ");
	organization.field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> Organization.show());
	train.field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> Train.show());
	fortify.field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> Fortify.show());
	missions.field.setOnMouseClicked(e -> comingSoon());
	
	options = new ArrayList<>(Arrays.asList(missions, organization, train, fortify));
	Game.coins();
	menu();
	stage.show();
    }
    
    private void comingSoon() {
	root.getChildren().clear();
	Text cs = new Text(275,310,"Coming Soon?");
	cs.setFont(Font.font(40));
	root.getChildren().add(cs);
	Option.back();
    }

    static void menu() {
	root.getChildren().clear();
	Game.grid();
	Game.coins();
	options.forEach(e -> e.show());
    }

    public static void main(String[] args) {
	launch();
    }
}
