package ir.ac.kntu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Buy {
    private static Rectangle NEBox;
    private static Text notEnough;
    private static List<Shape> toggle;

    public static void init() {
	notEnough = new Text(225, 325, "You dont have enough money");
	notEnough.setFont(Font.font(24));
	notEnough.setVisible(false);
	notEnough.setFill(Color.WHITE);
	notEnough.setMouseTransparent(true);
	NEBox = new Rectangle(200, 290, 375, 50);
	NEBox.setOpacity(0.3);
	NEBox.setVisible(false);
	NEBox.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> toggle.forEach(s -> s.setVisible(false)));
	Main.root.getChildren().addAll(notEnough, NEBox);
	toggle = new ArrayList<>(Arrays.asList(NEBox, notEnough));
    }

    public static void notEnough() {
	toggle.forEach(e -> e.setVisible(true));
    }
}
