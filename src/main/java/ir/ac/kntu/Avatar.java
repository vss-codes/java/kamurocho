package ir.ac.kntu;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Avatar {
    Ally player;
    double x;
    double y;
    boolean choosed;
    private Rectangle avatar;
    private Text shortName;
    private Text lvl;

    public Avatar(Ally player, double x, double y) {
	this.player = player;
	this.x = x;
	this.y = y;
	avatar = new Rectangle(x, y, 50, 50);
	avatar.setFill(Color.rgb(100, 0, 200));
	shortName = new Text(x + 10, y + 20, player.getShortName());
	shortName.setFont(Font.font(20));
	lvl = new Text(x + avatar.getWidth() - 25, y + avatar.getHeight() - 10, "Lv." + player.level);
	show();

	lvl.setMouseTransparent(true);
	shortName.setMouseTransparent(true);
	avatar.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
	    Organization.fullName.setText(player.name);
	    Organization.health.setText("Health: " + player.health);
	    Organization.attack.setText("Attack: " + player.attack);
	});
	avatar.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
	    if (!choosed && Organization.deck.size() < 9) {
		Organization.deck.add(this);
	    } else {
		go(this.x, this.y);
		Organization.deck.remove(this);
	    }
	    choosed = !choosed;
	    Organization.showDeck();
	});
    }

    public void show() {
	Main.root.getChildren().addAll(avatar, shortName, lvl);	
    }

    public void go(double x, double y) {
	avatar.setX(x);
	avatar.setY(y);
	shortName.setX(x + 10);
	shortName.setY(y + 20);
	lvl.setX(x + avatar.getWidth() - 25);
	lvl.setY(y + avatar.getHeight() - 10);
    }
}
