package ir.ac.kntu;

import javafx.scene.shape.Rectangle;

public abstract class HQ {
    int durability;
    int lvl = 1;
    protected Rectangle shape = new Rectangle();

    public void show(double x, double y) {
	Rectangle cur = Fortify.cur;
	cur.setX(x);
	cur.setY(y);
	cur.setWidth(shape.getWidth());
	cur.setHeight(shape.getHeight());
	cur.setFill(shape.getFill());
    }
}
