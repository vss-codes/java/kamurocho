package ir.ac.kntu;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Truck extends HQ {

    public Truck() {
	durability = 5000;
	shape.setWidth(25);
	shape.setHeight(100);
	shape.setFill(Color.RED);
	Main.root.getChildren().add(shape);
	shape.setVisible(false);
    }

    public Truck(double x, double y) {
	this();
	show(x, y);
    }
}
