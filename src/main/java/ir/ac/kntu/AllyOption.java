package ir.ac.kntu;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class AllyOption extends Option {
    private static Text name, health, newHealth, damage, newDamage;
    private Ally player;
    private Text level, cost;
    private int price;

    public AllyOption(double x, double y, String text, Ally player) {
	super(x, y, text);
	field.setWidth(400);
	this.player = player;
	price = player.level * 10;
	level = new Text(x + 200, y + 25, "lvl: " + player.level);
	level.setFont(Font.font(20));
	cost = new Text(x + 300, y + 25, "cost: " + price);
	cost.setFont(Font.font(20));
	level.setMouseTransparent(true);
	cost.setMouseTransparent(true);

	name = new Text(560, 175, "");
	name.setFont(Font.font(24));

	health = new Text(575, 225, "");
	health.setFont(Font.font(20));
	newHealth = new Text(572, 275, "");
	newHealth.setFill(Color.LIME);
	newHealth.setFont(Font.font(20));

	damage = new Text(675, 225, "");
	damage.setFont(Font.font(20));
	newDamage = new Text(672, 275, "");
	newDamage.setFill(Color.LIME);
	newDamage.setFont(Font.font(20));

	root.getChildren().addAll(level, cost, name, health, newHealth, damage, newDamage);

	field.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
//	    hq.show(500 + (100 - hq.shape.getWidth()) / 2, 100 + (100 - hq.shape.getHeight()) / 2);
	    name.setText(player.name);
	    health.setText("health: \n" + player.health);
	    newHealth.setText("+" + (int) Math.floor(player.health * 0.04));
	    damage.setText("damage: \n" + player.attack);
	    newDamage.setText("+" + (int) Math.floor(player.attack * 0.04));
	});

	field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
	    if (Game.coins >= price) {
		updgrade(this.player);
	    } else {
		Buy.notEnough();
	    }
	});
    }

    private void updgrade(Ally player) {
	player.health *= 1.04;
	player.attack *= 1.04;
	Game.coins -= price;
	Game.coins();
	price = ++player.level * 10;
	level.setText("lvl: " + player.level);
	cost.setText("cost: " + price);
    }

}
