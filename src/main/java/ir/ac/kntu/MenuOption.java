package ir.ac.kntu;

import javafx.scene.input.MouseEvent;

public class MenuOption extends Option {

    public MenuOption(int x, int y, String text) {
	super(x, y, text);
	field.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
	    root.getChildren().clear();
	    Game.grid();
	    Game.coins();
	    back();
	});
    }

}
