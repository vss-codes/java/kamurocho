package ir.ac.kntu;

import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

abstract class Option {
    static Group root = Main.root;
    Rectangle field;
    Text title;

    Option(double x, double y, String text) {
	field = new Rectangle(x, y, 300, 45);
	field.setFill(Color.rgb(100, 0, 100));
	title = new Text(x + 20, y + 25, text);
	title.setFont(Font.font(20));
	title.setMouseTransparent(true);
	show();
	field.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> field.setFill(Color.rgb(100, 0, 200)));
	field.addEventHandler(MouseEvent.MOUSE_EXITED, e -> field.setFill(Color.rgb(100, 0, 100)));

	Main.scene.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	    if (e.getCode() == KeyCode.ESCAPE) {
		Main.menu();
	    }
	});
    }

    public static void back() {
	Text back = new Text(50,50,"⬅");
	back.setFont(Font.font(50));
	back.setOnMouseClicked(e -> Main.menu());
	root.getChildren().add(back);
    }

    public void show() {
	root.getChildren().addAll(field, title);
    }
}
