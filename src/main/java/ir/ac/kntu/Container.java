package ir.ac.kntu;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Container extends HQ {

    public Container() {
	durability = 3000;
	shape.setWidth(50);
	shape.setHeight(50);
	shape.setFill(Color.SADDLEBROWN);
	Main.root.getChildren().add(shape);
	shape.setVisible(false);
    }

    public Container(double x, double y) {
	this();
	show(x, y);
    }
}
