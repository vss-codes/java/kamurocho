package ir.ac.kntu;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Fortify {
    static HQ truck;
    static HQ container;
    static HQ van;
    static Rectangle cur;
    public static void show() {  
	Organization.info.setFill(Color.BLUE);
	Main.root.getChildren().add(Organization.info);
	cur = new Rectangle();
	Main.root.getChildren().add(cur);
	truck = new Truck();        
	container = new Container();
	van = new Van();            
	new HqOption(100, 100, "container", container);
	new HqOption(100, 150, "truck", truck);
	new HqOption(100, 200, "van", van);
	Buy.init();
    }

}
