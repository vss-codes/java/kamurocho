package ir.ac.kntu;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Game {
    static List<Ally> players = new ArrayList<>();
    static List<Soldier> enemies = new ArrayList<>();
    static int coins = 100;
    static Group root = Main.root;
    static Text coin = new Text(577, 50, "");

    public static void grid() {
	for (int i = 1; i < 8; i++)
	    root.getChildren().add(new Line(100 * i, 0, 100 * i, 600));
	for (int i = 1; i < 6; i++)
	    root.getChildren().add(new Line(0, 100 * i, 800, 100 * i));
    }

    public static void coins() {
//	    root.getChildren().remove(coin);
	coin.setText(String.valueOf(coins) + "¥");
	coin.setFont(Font.font(24));
	if (!root.getChildren().contains(coin))
	    root.getChildren().add(coin);
    }
}
