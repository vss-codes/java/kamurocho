package ir.ac.kntu;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Train {

    static void show() {
	Rectangle info = new Rectangle(550, 150, 200, 150);
	info.setFill(Color.BLUE);
	Main.root.getChildren().add(info);
	for (int i = 0; i < Game.players.size(); i++) {
	    Ally player = Game.players.get(i);
	    new AllyOption(100, 5 + 50 * i, player.name, player);
	}
	Buy.init();
    }
}
