package ir.ac.kntu;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Organization {
    private static Group root = Main.root;
    private static List<Ally> players = Game.players;
    static Rectangle info = new Rectangle(500, 100, 200, 100);
    private static double x = info.getX();
    private static double y = info.getY();
    private static double h = info.getHeight();
    private static Rectangle deckR;
    static List<Avatar> deck = new ArrayList<>();
    static Text fullName = new Text(x + 10, y + 20, "");
    static Text health = new Text(x + 10, y + h - 10, "");
    static Text attack = new Text(x + 110, y + h - 10, "");

    public static void show() {
	deckR = new Rectangle(175, 325, 450, 50);
	deckR.setFill(Color.rgb(30, 136, 51));
	root.getChildren().add(deckR);
	deck.forEach(e -> e.show());
	showPlayers();
	root.getChildren().addAll(info, fullName, health, attack);

    }

    static void showDeck() {
	for (int i = 0; i < deck.size(); i++) {
	    deck.get(i).go(175 + 50 * i, 325);
	}

    }

    private static void showPlayers() {
	fullName.setFont(Font.font(20));
	info.setFill(Color.BLUE);
	for (int i = 0; i < 6; i++) {
	    for (int j = 0; j < 2; j++) {
		Ally player = players.get(i + 6 * j);
		if (!deck.stream().map(e -> e.player).collect(Collectors.toList()).contains(player))
		    new Avatar(player, 100 + 55 * i, 100 + 55 * j);
	    }
	}
	fullName.setText(players.get(0).name);
	health.setText("health: " + players.get(0).health);
    }
}
