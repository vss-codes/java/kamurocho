package ir.ac.kntu;

public class Ally extends Soldier {
    int FoV;

    public Ally(String name, int health, int dps, int Fov, int range) {
        super(name, health, dps, range);
        this.FoV = Fov;
    }
}
