package ir.ac.kntu;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Soldier {
    String name;
    int health;
    int attack;
    int range;
    int level=1;

    public Soldier(String name, int health, int attack, int range) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.range = range;
    }

    public static void createAll(List<Ally> players, List<Soldier> enemies) {
        players.addAll(Arrays.asList(
                new Ally("Goro Majima", 4000, 4500, 3, 4),
                new Ally("Daigo Dojima", 4000, 4000, 1, 2),
                new Ally("Kaoru Sayama", 4500, 4500, 2, 4),
                new Ally("Taiga Saejima", 7000, 5000, 1, 1),
                new Ally("Sohei Dojima", 3000, 3000, 1, 1),
                new Ally("Shintaro Kazama", 4500, 4500, 2, 1),
                new Ally("Ryuji Goda", 5000, 5000, 1, 2),
                new Ally("Osamu Kashiwagi", 4000, 3000, 2, 1),
                new Ally("Makoto Date", 4500, 1800, 2, 3),
                new Ally("Futoshi Shimano", 4200, 4000, 2, 1),
                new Ally("Ryo Takashima", 3600, 3800, 1, 1),
                new Ally("Jiro Kawara", 5500, 3200, 3, 3)
        ));
        
        enemies.addAll(Arrays.asList(
                new Soldier("Red Soldier", 1000, 500, 1),
                new Soldier("Grenadier", 800, 1800, 2),
                new Soldier("Batter", 1000, 800, 1),
                new Soldier("Boss 1", 8000, 5000, 1),
                new Soldier("Boss 2", 20000, 12000, 1)
        ));
    }

    public String getShortName() {
        return Stream.of(name.split(" "))
                     .map(s -> Character.toString(s.charAt(0)))
                     .collect(Collectors.joining());
    }
}
